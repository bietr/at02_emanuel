<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autor extends CI_Controller {
	public function index()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->view('autor/info.php');
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}
}
