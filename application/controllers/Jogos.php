<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jogos extends CI_Controller {
	public function index()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->model('JogoModel');
		$data['jogos'] = $this->JogoModel->get_data();
		$this->load->view('index.php', $data);
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}

	public function empresa()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->view('empresa/empresa_conteudo.php');
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}

	public function contato()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->view('contato/contato_conteudo.php');
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}

	public function api()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->view('api/info.php');
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}

	public function administrativo()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->model('JogoModel');
		$data['jogos'] = $this->JogoModel->get_data();
		$this->load->view('adm/lista.php', $data);
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}

	public function delete_jogo($id){
		$this->load->model('JogoModel');
		if ($this->JogoModel->delete_jogo($id)) {
			redirect(base_url('jogos/administrativo'));
		} else {
			echo '<script>alert("Algo deu errado! Tente novamente ou consulte um técnico!")</script>';
		}
	}
	public function alterar_jogo($id){
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->model('JogoModel');
		$data = $this->JogoModel->get_jogo($id);
		$this->JogoModel->editar_jogo($id);
		$this->load->view('adm/form', $data);
		$this->load->view('common/footer.php');
		$this->load->view('common/rodape.php');
	}
}
