<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-md-6">
			<form class="text-center p-5" method="POST">
				<p class="h4 mb-4">Alterar produto</p>
				<input type="text" name="nome" class="form-control mb-4" placeholder="Nome do Jogo" value="<?= $nome ?>">
				<input type="text" name="image" class="form-control mb-4" placeholder="Imagem do Jogo" value="<?= $image?>">
				<button class="btn btn-dark btn-block" type="submit">Alterar</button>
			</form>
		</div>
	</div>
</div>
