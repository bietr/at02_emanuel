<div class="container mt-2">
	<div class="row">
		<div class="card col-md-12">
			<div class="card-body">
				<h4 class="card-title text-center"><a>Google Custom Search</a></h4>
				<p class="card-text text-center">A Google Custom Search é é um serviço oferecido pelo Google no qual usuários, que podem ser pessoas ou empresas, marcam websites relevantes para suas áreas de conhecimento, ou criar links especializados dos quais os usuários poderão pesquisar de acordo com o tema do website. </p>
			</div>
		</div>
	</div>
</div>
