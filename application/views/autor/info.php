<div class="container mt-2">
	<div class="row">
		<div class="card col-md-12">
			<img width="80%" height="80%" class="card-img-top" src="<?= base_url('assets/img/author.jpg') ?>"
				 alt="Card image cap">
			<div class="card-body">
				<h4 class="card-title text-center"><a>Emanuel Ricardo Biet Resende</a></h4>
				<p class="card-text text-center">Programador JR da empresa ATMO Digital, Técnico em Informática para Internet pelo IFSP e Graduando em Análise e Desenvolvimento de Sistemas.</p>
				<p class="card-text text-center">Prontuário: GU3001601</p>
				<p class="card-text text-center">Tema do Trabalho: Jogos</p>
			</div>
		</div>
	</div>
</div>
