<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>IFSP Jogos</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
