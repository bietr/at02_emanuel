<nav class="navbar navbar-expand-lg navbar-dark bg-success">
	<a class="navbar-brand" href="<?=base_url('')?>">IFSP Jogos</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item mr-auto">
				<a class="nav-link" href="<?=base_url('autor/') ?>">Autor</a>
			</li>
			<li class="nav-item mr-auto">
				<a class="nav-link" href="<?=base_url('jogos/api') ?>">API</a>
			</li>
			<li class="nav-item ">
				<a class="nav-link" href="<?=base_url('')?>">Produtos</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?=base_url('jogos/empresa') ?>">Empresa</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?=base_url('jogos/contato') ?>">Contato</a>
			</li>
			<li class="nav-item mr-auto">
				<a class="nav-link" href="<?=base_url('jogos/administrativo') ?>">Administrativo</a>
			</li>
		</ul>
	</div>
</nav>
