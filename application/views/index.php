<div class="container">
   <h1 class="mt-5 gradient">Faça uma pesquisa em nosso acervo:</h1>
  <div class="row mt-2">
	  <div class="col-md-12">
		  <script>
			  (function() {
				  var cx = '017329541799294506369:syote1u1jdc';
				  var gcse = document.createElement('script');
				  gcse.type = 'text/javascript';
				  gcse.async = true;
				  gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
				  var s = document.getElementsByTagName('script')[0];
				  s.parentNode.insertBefore(gcse, s);
			  })();
		  </script>
		  <gcse:search></gcse:search>
	  </div>

	  <?php foreach($jogos as $jogo):?>
	  <div class="col-md-4 mb-3">
      <img src="assets/img/<?= $jogo->image ?>.jpg" class="rounded img-fluid"
        alt="AC Odyssey">
		<p class="text-center lead monospace"><?= $jogo->nome ?></p>
    </div>
	  <?php endforeach; ?>
<!--    <div class="col-md-4 mb-3">-->
<!--      <img src="assets/img/game2.jpg" class="rounded img-fluid"-->
<!--        alt="Red Dead Redemption 2">-->
<!--		<p class="text-center lead monospace">Red Dead Redemption 2</p>-->
<!--    </div>-->
<!--    <div class="col-md-4 mb-3">-->
<!--      <img src="assets/img/game3.jpg" class="rounded img-fluid"-->
<!--        alt="Resident Evil 2 Remake">-->
<!--		<p class="text-center lead monospace">Resident Evil 2 Remake</p>-->
<!--    </div>-->
  </div>
</div>
